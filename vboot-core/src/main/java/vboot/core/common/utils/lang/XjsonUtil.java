package vboot.core.common.utils.lang;

import vboot.core.common.mvc.pojo.Zinp;
import vboot.core.common.mvc.pojo.Ztree;

import java.util.ArrayList;
import java.util.List;

public class XjsonUtil {

    public static List<Ztree> buildTreeByRecursive(List<Ztree> nodes) {
        List<Ztree> list = new ArrayList<>();
        for (Ztree node : nodes) {
            if (node.getPid() == null) {
                list.add(findTreeChildrenByTier(node, nodes));
            } else {
                boolean flag = false;
                for (Ztree node2 : nodes) {
                    if (node.getPid().equals(node2.getId())) {
                        flag = true;
                        break;
                    }
                }
                if (!flag) {
                    list.add(findTreeChildrenByTier(node, nodes));
                }
            }
        }
        return list;
    }


    //递归查找子节点
    private static Ztree findTreeChildrenByTier(Ztree node, List<Ztree> nodes) {
        for (Ztree item : nodes) {
            if (node.getId().equals(item.getPid())) {
                if (node.getChildren() == null) {
                    node.setChildren(new ArrayList<>());
                }
                node.getChildren().add(findTreeChildrenByTier(item, nodes));
            }
        }
        return node;
    }


//    //使用递归方法建树
    public static List<Zinp> buildInpByRecursive(List<Zinp> nodes) {
        List<Zinp> list = new ArrayList<>();
        for (Zinp node : nodes) {
            if (node.getPid() == null) {
                list.add(findInpChildrenByTier(node, nodes));
            } else {
                boolean flag = false;
                for (Zinp node2 : nodes) {
                    if (node.getPid().equals(node2.getId())) {
                        flag = true;
                        break;
                    }
                }
                if (!flag) {
                    list.add(findInpChildrenByTier(node, nodes));
                }
            }
        }
        return list;
    }
//
//
//    //递归查找子节点
    private static Zinp findInpChildrenByTier(Zinp node, List<Zinp> nodes) {
        for (Zinp item : nodes) {
            if (node.getId().equals(item.getPid())) {
                if (node.getChildren() == null) {
                    node.setChildren(new ArrayList<>());
                }
                node.getChildren().add(findInpChildrenByTier(item, nodes));
            }
        }
        return node;
    }


//    //使用递归方法建树
//    public static List<Ztree> buildByRecursive(List<Ztree> treeNodes) {
//        List<Ztree> trees = new ArrayList<>();
//        for (Ztree treeNode : treeNodes) {
//            if (treeNode.getPid()==null) {
//                trees.add(findChildrenByTier(treeNode,treeNodes));
//            }
//        }
//        return trees;
//    }
//
//    //递归查找子节点
//    public static Ztree findChildrenByTier(Ztree treeNode, List<Ztree> treeNodes) {
////        treeNode.setChildren(new ArrayList<>());
//        for (Ztree it : treeNodes) {
//            if(treeNode.getId().equals(it.getPid())) {
//                if (treeNode.getChildren() == null) {
//                    treeNode.setChildren(new ArrayList<>());
//                }
//                treeNode.getChildren().add(findChildrenByTier(it,treeNodes));
//            }
//        }
//        return treeNode;
//    }

//    public static List<Ztree> inpToTree(List<Zinp> inpList) {
//        List<Ztree> trees = new ArrayList<>();
//        for (Zinp zinp : inpList) {
//            if (zinp.getPid()==null) {
//                trees.add(findChildrenByTree(zinp,inpList));
//            }
//        }
//        return trees;
//    }
//
//    private static Ztree findChildrenByTree(Zinp zinp, List<Zinp> zinpList) {
////        treeNode.setChildren(new ArrayList<>());
//        Ztree ztree =new Ztree();
//        ztree.setId(zinp.getId());
//        ztree.setName(zinp.getName());
//        for (Zinp it : zinpList) {
//            if(zinp.getId().equals(it.getPid())) {
//                if (ztree.getChildren() == null) {
//                    ztree.setChildren(new ArrayList<>());
//                }
//                ztree.getChildren().add(findChildrenByTree(it,zinpList));
//            }
//        }
//        return ztree;
//    }


}
