package vboot.core.common.mvc.api;

import java.util.ArrayList;
import java.util.HashMap;

//Rest Result结果返回
public class R extends HashMap<String, Object> {
    private static final long serialVersionUID = 1L;

    public static final String CODE_TAG = "code";

    public static final String MSG_TAG = "message";

    public static final String DATA_TAG = "result";

    public R() {

    }

    public R(int code, String msg) {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
    }

    public R(int code, String msg, Object data) {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
        super.put(DATA_TAG, data);
    }

    public static R ok() {
        return R.ok("操作成功");
    }

    public static R ok(Object data) {
        return R.ok("操作成功", data);
    }

    public static R empty() {
        return R.ok("操作成功", new ArrayList<>());
    }

    public static R ok(String msg, Object data) {
        return new R(0, msg, data);
    }

    public static R error(Object data) {
        return R.error("操作失败", data);
    }

    public static R error(String msg, Object data) {
        return new R(500, msg, data);
    }

    public static R build(int code, String msg) {
        return new R(code, msg, null);
    }

    public static R build(int code, String msg, Object data) {
        return new R(code, msg, data);
    }
}
