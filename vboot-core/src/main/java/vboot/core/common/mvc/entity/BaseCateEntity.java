package vboot.core.common.mvc.entity;

import io.swagger.annotations.ApiModelProperty;
import vboot.core.module.sys.org.root.SysOrg;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//分类Entity基类，可实现无限多级的树结构
@MappedSuperclass
@Getter
@Setter
public class BaseCateEntity {
    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    protected String id;

    //----------------------层级属性参考-----------------------
    @Column(length = 1000)
    @ApiModelProperty("层级信息")
    protected String tier;

    @Transient
    @ApiModelProperty("父分类ID")
    protected String pid;

    @Transient
    @ApiModelProperty("父分类")
    protected BaseCateEntity parent;

    @Transient
    @ApiModelProperty("子分类列表")
    protected List<BaseCateEntity> children = new ArrayList<>();
    //----------------------通用属性-----------------------
    @Column(length = 100)
    @ApiModelProperty("名称")
    protected String name;

    @Column(length = 64)
    @ApiModelProperty("标签")
    protected String label;

    @Column(length = 500)
    @ApiModelProperty("备注")
    protected String notes;

    @ApiModelProperty("可用标记 1启用，0禁用")
    protected Boolean avtag;

    @ApiModelProperty("排序号")
    protected Integer ornum;

    @ManyToOne
    @JoinColumn(name = "crman", updatable = false)
    @ApiModelProperty("创建人")
    protected SysOrg crman;

    @Column(updatable = false)
    @ApiModelProperty("创建时间")
    protected Date crtim = new Date();

    @ApiModelProperty("更新时间")
    protected Date uptim;

    @ManyToOne
    @JoinColumn(name = "upman")
    @ApiModelProperty("更新人")
    protected SysOrg upman;

}
