package vboot.core.framework.init;

import vboot.core.module.sys.org.user.SysOrgUserRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;

//数据库初始化监听器
@Configuration
@Slf4j
public class DbInitListener implements ApplicationListener<ContextRefreshedEvent> {

    @Value("${app.init.type}")
    private String INIT_TYPE;

    //首次启动，数据库生成后，初始化组织架构，菜单，权限角色,接口等信息
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        try {
            if (!userDao.existsById("sa")) {
                System.out.println("首次启动系统，正在进行数据库初始化，请耐心等待。");
                if ("demo".equals(INIT_TYPE)) {
                    sysOrgInit.initCorp();
                    System.out.println("1.1 初始化部门完毕");
                    sysOrgInit.initUser();
                    sysOrgInit.initZsf();
                }
                sysOrgInit.initSa();
                System.out.println("1.2 初始化用户完毕");
//                sysOrgInit.initPost();
//                sysOrgInit.initGroup();

                sysPortalInit.initPortal();
                sysPortalInit.initSysMenu();
                sysPortalInit.initSaMenu();

                System.out.println("2 初始化门户完毕");
                assInit.initData();
                System.out.println("3 初始化辅助数据完毕");
            }
            sysApiInit.initApi();

        } catch (Exception e) {
            System.err.println("初始化出错");
            e.printStackTrace();
        }
    }

    @Autowired
    private SysOrgUserRepo userDao;

    @Autowired
    private SysOrgInit sysOrgInit;

    @Autowired
    private AssInit assInit;

    @Autowired
    private SysApiInit sysApiInit;

    @Autowired
    private SysPortalInit sysPortalInit;

}