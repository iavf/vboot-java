package vboot.core.framework.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vboot.core.module.ass.dict.data.AssDictData;
import vboot.core.module.ass.dict.data.AssDictDataRepo;
import vboot.core.module.ass.dict.main.AssDictMain;
import vboot.core.module.ass.dict.main.AssDictMainRepo;
import vboot.core.module.ass.num.main.AssNumMain;
import vboot.core.module.ass.num.main.AssNumMainRepo;
import vboot.core.common.utils.lang.XstrUtil;

import java.util.ArrayList;
import java.util.List;

//辅助数据初始化
@Component
public class AssInit {

    protected void initData() throws Exception {
        initNum();
        initDict();
    }

    //流水号初始化
    private void initNum() {
        List<AssNumMain> numList = new ArrayList<>();
        AssNumMain suppNum = new AssNumMain();
        suppNum.setId("SUPP");
        suppNum.setName("供应商流水号");
        suppNum.setNumod("yyyymmdd");
        suppNum.setNulen(3);
        suppNum.setNflag(true);
        suppNum.setNupre("SU");
        numList.add(suppNum);

        AssNumMain projNum = new AssNumMain();
        projNum.setId("PROJ");
        projNum.setName("项目流水号");
        projNum.setNumod("yyyymmdd");
        projNum.setNulen(3);
        projNum.setNflag(true);
        projNum.setNupre("PR");
        numList.add(projNum);

        AssNumMain custNum = new AssNumMain();
        custNum.setId("CUST");
        custNum.setName("客户流水号");
        custNum.setNumod("yyyymmdd");
        custNum.setNulen(3);
        custNum.setNflag(true);
        custNum.setNupre("CU");
        numList.add(custNum);

        AssNumMain agentNum = new AssNumMain();
        agentNum.setId("AGENT");
        agentNum.setName("代理商流水号");
        agentNum.setNumod("yyyymmdd");
        agentNum.setNulen(3);
        agentNum.setNflag(true);
        agentNum.setNupre("AG");
        numList.add(agentNum);
        numRepo.saveAll(numList);

    }

    //数据字典初始化
    private void initDict() {
        List<AssDictMain> dictList = new ArrayList<>();

        AssDictMain suppDict = new AssDictMain();
        suppDict.setId("SU_LEVEL");
        suppDict.setName("供应商资质等级");
        suppDict.setAvtag(true);
        dictList.add(suppDict);

        AssDictMain agentDict = new AssDictMain();
        agentDict.setId("AG_LEVEL");
        agentDict.setName("代理商资质等级");
        agentDict.setAvtag(true);
        dictList.add(agentDict);

        dictRepo.saveAll(dictList);

        List<AssDictData> dictDataList = new ArrayList<>();
        AssDictData supp1 = new AssDictData();
        supp1.setId(XstrUtil.getUUID());
        supp1.setCode("A");
        supp1.setName("A级资质");
        supp1.setAvtag(true);
        supp1.setOrnum(1);
        supp1.setMaiid("SU_LEVEL");
        dictDataList.add(supp1);

        AssDictData supp2 = new AssDictData();
        supp2.setId(XstrUtil.getUUID());
        supp2.setCode("B");
        supp2.setName("B级资质");
        supp2.setAvtag(true);
        supp2.setOrnum(2);
        supp2.setMaiid("SU_LEVEL");
        dictDataList.add(supp2);

        AssDictData supp3 = new AssDictData();
        supp3.setId(XstrUtil.getUUID());
        supp3.setCode("C");
        supp3.setName("C级资质");
        supp3.setAvtag(true);
        supp3.setOrnum(3);
        supp3.setMaiid("SU_LEVEL");
        dictDataList.add(supp3);

        AssDictData supp4 = new AssDictData();
        supp4.setId(XstrUtil.getUUID());
        supp4.setCode("Z");
        supp4.setName("不合格");
        supp4.setAvtag(true);
        supp4.setOrnum(4);
        supp4.setMaiid("SU_LEVEL");
        dictDataList.add(supp4);

        AssDictData agent1 = new AssDictData();
        agent1.setId(XstrUtil.getUUID());
        agent1.setCode("A");
        agent1.setName("A级资质");
        agent1.setAvtag(true);
        agent1.setOrnum(1);
        agent1.setMaiid("AG_LEVEL");
        dictDataList.add(agent1);

        AssDictData agent2 = new AssDictData();
        agent2.setId(XstrUtil.getUUID());
        agent2.setCode("B");
        agent2.setName("B级资质");
        agent2.setAvtag(true);
        agent2.setOrnum(2);
        agent2.setMaiid("AG_LEVEL");
        dictDataList.add(supp2);

        AssDictData agent3 = new AssDictData();
        agent3.setId(XstrUtil.getUUID());
        agent3.setCode("C");
        agent3.setName("C级资质");
        agent3.setAvtag(true);
        agent3.setOrnum(3);
        agent3.setMaiid("AG_LEVEL");
        dictDataList.add(agent3);

        AssDictData agent4 = new AssDictData();
        agent4.setId(XstrUtil.getUUID());
        agent4.setCode("Z");
        agent4.setName("不合格");
        agent4.setAvtag(true);
        agent4.setOrnum(4);
        agent4.setMaiid("AG_LEVEL");
        dictDataList.add(agent4);

        dictDataRepo.saveAll(dictDataList);
    }

    @Autowired
    private AssDictMainRepo dictRepo;

    @Autowired
    private AssDictDataRepo dictDataRepo;

    @Autowired
    private AssNumMainRepo numRepo;
}
