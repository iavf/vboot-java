package vboot.core.framework.security.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

//用户名与密码VO，登录时用到
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private String username;

    @NotNull
    private String password;
}
