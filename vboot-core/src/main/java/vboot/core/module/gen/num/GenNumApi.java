package vboot.core.module.gen.num;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.api.R;
import vboot.core.common.utils.lang.XstrUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("gen/num")
@Api(tags = {"编号管理"})
public class GenNumApi {

    @GetMapping("uuid")
    @ApiOperation("获取唯一的UUID")
    public R uuid() {
        return R.ok(XstrUtil.getUUID());
    }

}
