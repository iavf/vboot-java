package vboot.core.module.gen.file;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.module.ass.file.att.AssFileAtt;
import vboot.core.module.ass.file.att.AssFileAttRepo;
import vboot.core.module.ass.file.att.AssFileAttHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("gen/att")
@Api(tags = {"附件管理"})
public class GenAttApi {

    @PostMapping(value="up",produces = "text/html;charset=UTF-8")
    @ApiOperation("附件上传")
    public String upload(@RequestParam(value = "file", required = false) MultipartFile file) throws Exception {
        System.out.println("上传xxxxxx");
        AssFileAtt att = handler.saveFile(file);
        repo.save(att);
        return "{\"id\":\"" + att.getId() + "\",\"addre\":\"" + att.getAddre() + "\",\"pname\":\"" + att.getPname() + "\",\"sname\":\"" + att.getSname() + "\",\"zsize\":\"" + att.getZsize() + "\",\"zimg\":\"" + att.getZimg() + "\"}";
    }

    @GetMapping(value="one/{id}")
    @ApiOperation("通过ID下载附件")
    public void getOne(@PathVariable String id, HttpServletRequest request, HttpServletResponse response) throws Exception {
        AssFileAtt att=repo.findById(id).get();
        handler.download(request,response,att);
    }

    @GetMapping(value="pdf")
    @ApiOperation("查看PDF附件")
    public void pdf(String f, HttpServletRequest request, HttpServletResponse response) throws Exception {
        AssFileAtt att=repo.findById(f).get();
        handler.download(request,response,att);
    }

    @GetMapping(value="path")
    @ApiOperation("通过地址下载附件")
    public void downloadByPath(String name, String path, HttpServletRequest request, HttpServletResponse response) throws Exception {
        handler.downloadByPath(request,response,name,path);
    }

    @Autowired
    private AssFileAttRepo repo;

    @Autowired
    private AssFileAttHandler handler;

}
