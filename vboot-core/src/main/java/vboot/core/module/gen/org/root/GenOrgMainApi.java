package vboot.core.module.gen.org.root;

import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.pojo.ZidName;
import vboot.core.common.utils.lang.XstrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("gen/org/main")
public class GenOrgMainApi {

    //根据部门ID，查询下级所有的部门,岗位,用户
    @GetMapping()
    public R get(String deptid, Integer type, String name) {
        List<ZidName> mapList = new ArrayList<>();
        if(XstrUtil.isBlank(name)&& XstrUtil.isBlank(deptid)){
            return R.ok(mapList);
        }
        if ((type & 2) != 0) {//部门
            Sqler deptSqler = new Sqler("sys_org_dept");
            if(XstrUtil.isNotBlank(name)){
                deptSqler.addLike("t.name", name);
            }else{
                deptSqler.addEqual("t.pid", deptid);
            }
            mapList.addAll(jdbcDao.findIdNameList(deptSqler));
        }
        if ((type & 4) != 0) {//岗位
            Sqler postSqler = new Sqler("sys_org_post");
            if(XstrUtil.isNotBlank(name)){
                postSqler.addLike("t.name", name);
            }else{
                postSqler.addEqual("t.deptid", deptid);
            }
            mapList.addAll(jdbcDao.findIdNameList(postSqler));
        }
        if ((type & 8) != 0) {//用户
            Sqler userSqler = new Sqler("sys_org_user");
            if(XstrUtil.isNotBlank(name)){
                userSqler.addLike("t.name", name);
            }else{
                userSqler.addEqual("t.deptid", deptid);
            }
            mapList.addAll(jdbcDao.findIdNameList(userSqler));
        }
        return R.ok(mapList);
    }

    @Autowired
    private JdbcDao jdbcDao;


}
