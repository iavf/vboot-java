package vboot.core.module.gen.sys.org;

import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.utils.lang.XstrUtil;
import vboot.core.framework.security.pojo.Zuser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("gen/org")
public class GenOrgApi {
    @GetMapping(value="watag")
    public R getWatag(HttpServletRequest request) {
        Zuser zuser = null;
        String sql = "update sys_org_user set watag=0 where id=?";
        jdbcDao.update(sql, zuser.getId());
        zuser.setWatag(false);
        return R.ok();
    }

    @GetMapping(value="choose")
    public R getChoose(String name, String type) {
        Sqler sqlHelper = new Sqler("id,pid,name","sys_org_main");
        sqlHelper.addWhere("t.avtag = 1");
        sqlHelper.addWhere(XstrUtil.isNotBlank(type),"type in ("+type+")");
        sqlHelper.addLike("name",name);
        sqlHelper.addEqual("t.label", "ekp");
        return R.ok(jdbcDao.findInpList(sqlHelper));
    }

    @GetMapping("search")
    public R getSearch(String name, String type) {
        Sqler sqlHelper = new Sqler("t.id,t.name","sys_org_main");
        sqlHelper.addLeftJoin("t2.name as pid", "sys_org t2", "t2.id=t.pid");
        sqlHelper.addWhere("t.avtag = 1");
        sqlHelper.addLike("t.name",name);
        sqlHelper.addWhere(XstrUtil.isNotBlank(type),"t.type in ("+type+")");
        return R.ok(jdbcDao.findInpList(sqlHelper));
    }


    @GetMapping("dept/{id}")
    public R getDept(@PathVariable String id, String type) {
        Sqler sqlHelper = new Sqler("t.id,t.name","sys_org_main");
        sqlHelper.addLeftJoin("t2.name as pid", "sys_org t2", "t2.id=t.pid");
        sqlHelper.addWhere("t.avtag = 1");
        sqlHelper.addWhere(XstrUtil.isNotBlank(type),"t.type in ("+type+")");
        sqlHelper.addWhere("t.pid = ?",id);
        return R.ok(jdbcDao.findInpList(sqlHelper));
    }

    @Autowired
    private JdbcDao jdbcDao;
}
