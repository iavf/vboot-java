package vboot.core.module.mon.job.root;

import vboot.core.module.mon.job.log.MonJobLog;
import vboot.core.module.mon.job.log.MonJobLogRepo;
import vboot.core.module.mon.job.main.MonJobMain;
import vboot.core.common.utils.lang.XstrUtil;
import vboot.core.common.utils.lang.XtimeUtil;
import vboot.core.common.utils.web.XspringUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.util.ReflectionUtils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;

public class MonJobFactory implements Job {
    @Override
    public void execute(JobExecutionContext context) {
        MonJobMain job = (MonJobMain) context.getMergedJobDataMap().get("job");
        Method method = ReflectionUtils.findMethod(XspringUtil.getBean(job.getJgroup()).getClass(), job.getJid());
        MonJobLog log = new MonJobLog();
        log.setId(XstrUtil.getUUID());
        MonJobLogRepo syJobMlogDao= (MonJobLogRepo) XspringUtil.getBean("monJobLogRepo");
        log.setName(job.getName());
        long start = System.currentTimeMillis();
        try {
            log.setSttim(XtimeUtil.getTime());
            method.invoke(XspringUtil.getBean(job.getJgroup()));
            log.setRet("成功");
            log.setMsg("用时：" + (System.currentTimeMillis() - start) / 1000 + "秒");
            log.setEntim(XtimeUtil.getTime());
            syJobMlogDao.save(log);
        } catch (Exception e) {
            System.err.println("定时任务运行失败");
            e.printStackTrace();
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw, true));
            log.setRet("失败");
            String message = sw.toString();
            if (message.length() >= 5000)
            {
                log.setMsg(message.substring(0, 5000));
            } else
            {
                log.setMsg(message);
            }
            log.setEntim(XtimeUtil.getTime());
            syJobMlogDao.save(log);
        }
    }


}