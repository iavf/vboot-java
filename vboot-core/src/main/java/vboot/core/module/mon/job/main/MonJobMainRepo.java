package vboot.core.module.mon.job.main;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MonJobMainRepo extends JpaRepository<MonJobMain,String> {

}

