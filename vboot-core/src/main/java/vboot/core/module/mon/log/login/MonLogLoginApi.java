package vboot.core.module.mon.log.login;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.api.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("mon/log/login")
@Api(tags = {"登录日志"})
public class MonLogLoginApi {

    @GetMapping
    @ApiOperation("查询登录日志分页")
    public R get(String name) {
        Sqler sqler = new Sqler("t.id,t.name,t.usnam,t.ip,t.crtim,t.addre,t.usession,t.onkey", "mon_log_login");
        sqler.addSelect("t.ageos,t.agbro,t.agdet");
        sqler.addLike("t.name", name);
        sqler.addOrder("t.crtim desc");
        return R.ok(jdbcDao.findPageData(sqler));
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询登录日志详情")
    public R getOne(@PathVariable String id) {
        return R.ok(repo.findById(id).get());
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除登录日志")
    public R delete(@PathVariable String[] ids)  {
        for (String id : ids) {
            repo.deleteById(id);
        }
        return R.ok();
    }

    @DeleteMapping("all")
    @ApiOperation("清空登录日志")
    public R deleteAll()  {
        repo.deleteAll();
        return R.ok();
    }

    @Autowired
    private JdbcDao jdbcDao;

    @Autowired
    private MonLogLoginRepo repo;
}
