package vboot.core.module.mon.log.login;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MonLogLoginRepo extends JpaRepository<MonLogLogin,String> {

}
