package vboot.core.module.sys.org.user;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.utils.lang.XstrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("sys/org/user")
@Api(tags = {"组织架构-用户管理"})
public class SysOrgUserApi {

    private String table = "sys_org_user";

    @GetMapping
    @ApiOperation("查询用户分页")
    public R get(String deptid, String name, String usnam, String ninam, String monum) {
        Sqler sqler = new Sqler(table);
        if (XstrUtil.isNotBlank(name)|| XstrUtil.isNotBlank(usnam)|| XstrUtil.isNotBlank(ninam)|| XstrUtil.isNotBlank(monum)) {
            sqler.addLike("t.name", name);
            sqler.addLike("t.usnam", usnam);
            sqler.addLike("t.ninam", ninam);
            sqler.addLike("t.monum", monum);
        } else if (XstrUtil.isNotBlank(deptid)){
            sqler.addEqual("t.deptid", deptid);
        }
        sqler.addSelect("t.crtim,t.uptim,t.deptid,t.notes");
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询用户详情")
    public R getOne(@PathVariable String id, HttpServletRequest request) {
        SysOrgUser main = service.findById(id);
        return R.ok(main);
    }

    @PostMapping
    @ApiOperation("新增用户")
    public R post(@RequestBody SysOrgUser main) {
        return R.ok(service.insert(main));
    }

    @PutMapping
    @ApiOperation("修改用户")
    public R put(@RequestBody SysOrgUser main) {
        return R.ok(service.update(main));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除用户")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }


    @Autowired
    private SysOrgUserService service;

}
