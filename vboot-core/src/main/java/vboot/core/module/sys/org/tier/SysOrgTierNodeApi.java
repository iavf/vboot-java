package vboot.core.module.sys.org.tier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.pojo.Ztree;
import vboot.core.common.utils.lang.XstrUtil;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("sys/org/tnode")
public class SysOrgTierNodeApi {

    private String table = "sys_org_tier_node";

    @GetMapping
    public R get(String name, String pid, String notes) {
        Sqler sqler = new Sqler(table);
        if (XstrUtil.isNotBlank(name)) {
            sqler.addLike("t.name" , name);
        } else {
            if ("".equals(pid)) {
                sqler.addWhere("t.pid is null");
            } else if (XstrUtil.isNotBlank(pid)) {
                sqler.addEqual("t.pid" , pid);
            }
        }
        sqler.addLike("t.notes" , notes);
        sqler.addWhere("t.avtag=1");
        sqler.addOrder("t.ornum");
        sqler.addSelect("t.ornum,t.notes,t.pid,t.crtim,t.uptim");
        return R.ok(jdbcDao.findPageData(sqler));
    }

    //查询层级树
    @GetMapping("tree")
    public R getTree(String name) {
        Sqler sqler = new Sqler("sys_org_tier_node");
        sqler.addSelect("t.pid");
        sqler.addLike("t.name", name);
        sqler.addOrder("t.ornum");
        sqler.addWhere("t.avtag=1");
        List<Ztree> list = jdbcDao.findTreeList(sqler);
        return R.ok(list);
    }

    //排除了自己
//    @GetMapping("listn")
//    public R listn(String name, String id) {
//        Sqler sqler = new Sqler(table);
//        sqler.addLike("t.name", name);
//        sqler.addWhere("t.avtag=1");
//        sqler.addOrder("t.ornum");
//        sqler.addSelect("t.pid");
//        return R.ok(service.findWithoutItself(sqler, id));
//    }

    @GetMapping("one/{id}")
    public R getOne(@PathVariable String id, HttpServletRequest request) {
        SysOrgTierNode main = service.findById(id);
        return R.ok(main);
    }


    @PostMapping
    public R post(@RequestBody SysOrgTierNode main) {
        service.insert(main);
        return R.ok(main.getId());
    }

    @PutMapping
    public R put(@RequestBody SysOrgTierNode main) throws Exception {
        service.update(main);
        return R.ok(main.getId());
    }

    @DeleteMapping("{ids}")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private JdbcDao jdbcDao;

    @Autowired
    private SysOrgTierNodeService service;

}
