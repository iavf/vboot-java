package vboot.core.module.sys.org.post;

import cn.hutool.core.util.StrUtil;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.module.sys.org.root.SysOrg;
import vboot.core.common.mvc.api.PageData;
import vboot.core.common.utils.lang.XstrUtil;
import vboot.core.module.sys.org.root.SysOrgRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
@Transactional(rollbackFor = Exception.class)
public class SysOrgPostService {

    public PageData findPageData(Sqler sqler) {
        return jdbcDao.findPageData(sqler);
    }


    public String insert(SysOrgPost main) {
        if (main.getId() == null || "".equals(main.getId())) {
            main.setId(XstrUtil.getUUID());
        }
        if (main.getDept() == null || StrUtil.isBlank(main.getDept().getId())) {
            main.setTier("x" + main.getId() + "x");
        } else {
            String tier = jdbcDao.findOneString("select tier from sys_org_dept where id=?", main.getDept().getId());
            main.setTier(tier + main.getId() + "x");
        }
        SysOrg sysOrg = new SysOrg(main.getId(), main.getName());
        orgRepo.save(sysOrg);
        postRepo.save(main);
        return main.getId();
    }

    public String update(SysOrgPost main) {
        main.setUptim(new Date());
        SysOrg sysOrg = new SysOrg(main.getId(), main.getName());
        if (main.getDept() == null || StrUtil.isBlank(main.getDept().getId())) {
            main.setTier("x" + main.getId() + "x");
        } else {
            String tier = jdbcDao.findOneString("select tier from sys_org_dept where id=?", main.getDept().getId());
            main.setTier(tier + main.getId() + "x");
        }
        SysOrgPost backPost = postRepo.save(main);
        orgRepo.save(sysOrg);
        return backPost.getId();
    }

    public int delete(String[] ids) {
        for (String id : ids) {
            postRepo.deleteById(id);
            orgRepo.deleteById(id);
        }
        return ids.length;
    }

    @Transactional(readOnly = true)
    public SysOrgPost findById(String id) {
        return postRepo.findById(id).get();
    }

    @Autowired
    private JdbcDao jdbcDao;


    @Autowired
    private SysOrgRepo orgRepo;

    @Autowired
    private SysOrgPostRepo postRepo;


}
