package vboot.core.module.sys.org.post;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import vboot.core.module.sys.org.root.SysOrg;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(indexes = {@Index(columnList = "tier",unique = true), @Index(columnList = "avtag")})
@ApiModel("组织架构岗位")
public class SysOrgPost{
    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    private String id;

    @Column(length = 32)
    @ApiModelProperty("名称")
    private String name;

    @Transient
    @ApiModelProperty("部门ID")
    private String deptid;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "deptid")
    @ApiModelProperty("部门")
    private SysOrg dept;

    @Column(length = 1000)
    @ApiModelProperty("层级")
    private String tier;

    @ApiModelProperty("备注")
    private String notes;

    @ApiModelProperty("排序号")
    private Integer ornum;

    @ApiModelProperty("可用标记")
    private Boolean avtag;

    @Column(updatable = false)
    @ApiModelProperty("创建时间")
    private Date crtim = new Date();

    @ApiModelProperty("更新时间")
    private Date uptim;

    @Column(length = 1000)
    @ApiModelProperty("ldap层级名称")
    private String ldnam;

    @ManyToMany
    @JoinTable(name = "sys_org_post_org", joinColumns = {@JoinColumn(name = "pid")},
            inverseJoinColumns = {@JoinColumn(name = "oid")})
    @ApiModelProperty("员工列表")
    private List<SysOrg> users;


}
