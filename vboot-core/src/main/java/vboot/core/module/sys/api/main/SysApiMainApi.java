package vboot.core.module.sys.api.main;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.pojo.Zinp;
import vboot.core.common.utils.lang.XstrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("sys/api/main")
@Api(tags = {"接口管理-接口信息"})
public class SysApiMainApi {

    @GetMapping("tree")
    @ApiOperation("查询接口树数据")
    public R getTree(String id) {
        Sqler sqler = new Sqler("t.id,t.id as name,t.pid","sys_api_main");
        sqler.addLike("t.id", id);
        sqler.addWhere("t.avtag=1");
        List<SysApiMain> list = service.findTree(sqler);
        return R.ok(list);
    }

    @GetMapping
    @ApiOperation("查询接口分页")
    public R get(String id) {
        Sqler sqler = new Sqler("t.id,t.url,t.type", "sys_api_main");
        sqler.addLike("t.id", id);
        sqler.addOrder("t.crtim");
        sqler.addSelect("t.notes,t.crtim");
        return R.ok(jdbcDao.findPageData(sqler));
    }

    @GetMapping("list")
    @ApiOperation("查询接口列表")
    public R list(String name) {
        String sql="select * from sys_api_main where avtag=1";
        return R.ok( jdbcDao.findMapList(sql));
    }

    @GetMapping("type")
    @ApiOperation("根据分类查询接口树")
    public R type(String type) {
        if(XstrUtil.isNotBlank(type)){
            List<Zinp> stores=jdbcDao.findInpList("select id,pid from sys_api_main where pid like ?",type+"%");
            return R.ok(stores);
        }else{
            List<Zinp> stores=jdbcDao.findInpList("select id,pid from sys_api_main");
            return R.ok(stores);
        }
    }

    @GetMapping("one/{id}")
    @ApiOperation("根据接口详情")
    public R getOne(@PathVariable String id) {
        SysApiMain main=repo.findById(id).get();
        return R.ok(main);
    }

    @PostMapping
    @ApiOperation("新增接口")
    public R post(@RequestBody SysApiMain main) {
        main.setAvtag(true);
//        main.setType("");
        repo.save(main);
        return R.ok();
    }

    @PutMapping
    @ApiOperation("修改接口")
    public R put(@RequestBody SysApiMain main) {
        main.setUptim(new Date());
        repo.save(main);
        return R.ok();
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除接口")
    public R delete(@PathVariable String[] ids) {
        for (String id : ids) {
            repo.deleteById(id);
        }
        return R.ok();
    }

    @Autowired
    private JdbcDao jdbcDao;

    @Autowired
    private SysApiMainRepo repo;

    @Autowired
    private SysApiMainService service;
}
