package vboot.core.module.sys.org.tier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;

@RestController
@RequestMapping("sys/org/ttree")
public class SysOrgTierTreeApi {

    @GetMapping
    public R get(String name) {
        Sqler sqler = new Sqler("sys_org_tier_tree");
        sqler.addLike("t.name", name);
        sqler.addSelect("t.notes");
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("one/{id}")
    public R getOne(@PathVariable String id) {
        SysOrgTierTree main = service.findOne(id);
        return R.ok(main);
    }

    @PostMapping
    public R post(@RequestBody SysOrgTierTree main) {
        return R.ok(service.insert(main));
    }

    @PutMapping
    public R put(@RequestBody SysOrgTierTree main) {
        return R.ok(service.update(main));
    }

    @DeleteMapping("{ids}")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private SysOrgTierTreeService service;

}
