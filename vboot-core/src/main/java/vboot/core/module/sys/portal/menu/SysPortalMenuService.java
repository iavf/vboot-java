package vboot.core.module.sys.portal.menu;

import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.utils.lang.XstrUtil;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
public class SysPortalMenuService {

    @Transactional(readOnly = true)
    public SysPortalMenu findById(String id) {

        return repo.findById(id).get();
    }

    public String insert(SysPortalMenu main) {
        if (main.getId() == null || "".equals(main.getId())) {
            main.setId(XstrUtil.getUUID());
        }
        repo.save(main);
        return main.getId();
    }


    public SysPortalMenu update(SysPortalMenu main) {
        return repo.save(main);
    }

    public int delete(String[] ids) {
        Set<String> menuSet = new HashSet<>();
        for (String id : ids) {
            menuSet.add(id);
            List<SysPortalMenu> menuList = repo.findByPid(id);
            addChild(menuList, menuSet);
        }
        for (String str : menuSet) {
            repo.deleteById(str);
        }
        return menuSet.size();
    }

    public void addChild(List<SysPortalMenu> menuList, Set<String> menuSet) {
        for (SysPortalMenu menu : menuList) {
            menuSet.add(menu.getId());
            List<SysPortalMenu> menus = repo.findByPid(menu.getId());
            if (menus != null && menus.size() != 0) {
                addChild(menus, menuSet);
            }
        }
    }

    public List<SysPortalMenu> findTree(Sqler sqler) {
        List<SysPortalMenu> list = jdbcDao.getTp().query(sqler.getSql(), sqler.getParams(), new BeanPropertyRowMapper<>(SysPortalMenu.class));
        return buildByRecursive(list);
    }


    public List<SysPortalMenu> findAll(String name) {
        List<SysPortalMenu> list;
        if (StrUtil.isNotBlank(name)) {
            list = repo.findByNameLikeOrderByOrnum("%" + name + "%");
        } else {
            list = repo.findByOrderByOrnum();
        }
        return buildByRecursive(list);
    }


    public List<SysPortalMenu> findWithoutItself(String id) {
        List<SysPortalMenu> list = repo.findByOrderByOrnum();
        List<SysPortalMenu> menus = buildByRecursiveWithoutItself(list, id);

        return menus;
    }


    //高性能转换，但是要求顶层有pid,这里假设为0
    public static List<SysPortalMenu> buildTree(List<SysPortalMenu> pidList) {
        Map<String, List<SysPortalMenu>> pidListMap = pidList.stream().collect(Collectors.groupingBy(SysPortalMenu::getPid));
        pidList.stream().forEach(item -> item.setChildren(pidListMap.get(item.getId())));
        //返回结果也改为返回顶层节点的list
        return pidListMap.get("0");
    }

    //使用递归方法建树
    private List<SysPortalMenu> buildByRecursive(List<SysPortalMenu> nodes) {
        List<SysPortalMenu> list = new ArrayList<>();
        for (SysPortalMenu node : nodes) {
            if (node.getPid() == null) {
                list.add(findChildrenByTier(node, nodes));
            } else {
                boolean flag = false;
                for (SysPortalMenu node2 : nodes) {
                    if (node.getPid().equals(node2.getId())) {
                        flag = true;
                        break;
                    }
                }
                if (!flag) {
                    list.add(findChildrenByTier(node, nodes));
                }
            }
        }
        return list;
    }

    //递归查找子节点
    private SysPortalMenu findChildrenByTier(SysPortalMenu node, List<SysPortalMenu> nodes) {
        for (SysPortalMenu item : nodes) {
            if (node.getId().equals(item.getPid())) {
                if (node.getChildren() == null) {
                    node.setChildren(new ArrayList<>());
                }
                node.getChildren().add(findChildrenByTier(item, nodes));
            }
        }
        return node;
    }


    //使用递归方法建树不包含自己
    private List<SysPortalMenu> buildByRecursiveWithoutItself(List<SysPortalMenu> nodes, String id) {
        List<SysPortalMenu> list = new ArrayList<>();
        for (SysPortalMenu node : nodes) {
            if (node.getPid() == null && !node.getId().equals(id)) {
                list.add(findChildrenByTierWithoutItself(node, nodes, id));
            } else {
                boolean flag = false;
                for (SysPortalMenu node2 : nodes) {
                    if (node.getPid() != null&&node.getPid().equals(node2.getId())) {
                        flag = true;
                        break;
                    }
                }
                if (!flag && !node.getId().equals(id)) {
                    list.add(findChildrenByTierWithoutItself(node, nodes, id));
                }
            }
        }
        return list;
    }

    //递归查找子节点不包含自己
    private SysPortalMenu findChildrenByTierWithoutItself(SysPortalMenu node, List<SysPortalMenu> nodes, String id) {
        for (SysPortalMenu item : nodes) {
            if (node.getId().equals(item.getPid()) && (!item.getId().equals(id))) {
                if (node.getChildren() == null) {
                    node.setChildren(new ArrayList<>());
                }
                node.getChildren().add(findChildrenByTierWithoutItself(item, nodes, id));
            }
        }
        return node;
    }


    @Autowired
    private SysPortalMenuRepo repo;

    @Autowired
    private JdbcDao jdbcDao;
}
