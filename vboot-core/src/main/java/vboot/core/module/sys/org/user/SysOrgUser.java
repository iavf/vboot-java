package vboot.core.module.sys.org.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import vboot.core.module.sys.org.root.SysOrg;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(indexes = {@Index(columnList = "usnam",unique = true),@Index(columnList = "tier",unique = true), @Index(columnList = "avtag")})
@ApiModel("组织架构用户")
public class SysOrgUser {
    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    private String id;

    @Column(length = 32)
    @ApiModelProperty("名称")
    private String name;

    @Column(length = 32)
    @ApiModelProperty("昵称")
    private String ninam;

    @Transient
    @ApiModelProperty("部门ID")
    private String deptid;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "deptid")
    @ApiModelProperty("部门")
    private SysOrg dept;

    @Column(length = 1000)
    @ApiModelProperty("层级")
    private String tier;

    @Column(length = 200)
    @ApiModelProperty("职务")
    private String job;

    @Column(length = 32)
    @ApiModelProperty("登录名")
    private String usnam;

    @Column(length = 64,updatable = false)
    @JsonIgnore
    @ApiModelProperty("密码")
    private String pacod;

    @Column(length = 64)
    @ApiModelProperty("邮箱")
    private String email;

    @Column(length = 32)
    @ApiModelProperty("手机号")
    private String monum;

    @ApiModelProperty("提醒标记")
    private Boolean watag = false;

    @ApiModelProperty("备注")
    private String notes;

    @ApiModelProperty("排序号")
    private Integer ornum;

    @ApiModelProperty("可用标记")
    private Boolean avtag;

    @Column(length = 1000)
    @ApiModelProperty("ldap层级名称")
    private String ldnam;

    @ApiModelProperty("准备标记")
    private Boolean retag = false;

    public SysOrgUser() {

    }

    @Column(updatable = false)
    @ApiModelProperty("创建时间")
    private Date crtim = new Date();

    @ApiModelProperty("更新时间")
    private Date uptim;
}
