package vboot.core.module.sys.portal.menu;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("sys/portal/menu")
@Api(tags = {"门户管理-菜单信息"})
public class SysPortalMenuApi {

    @GetMapping("tree")
    @ApiOperation("查询菜单树")
    public R getTree(String name, String porid) {
        Sqler sqler = new Sqler("sys_portal_menu");
        sqler.addLike("t.name", name);
        sqler.addOrder("t.ornum");
        sqler.addWhere("t.avtag=1");
        sqler.addSelect("t.type,t.crtim,t.uptim,t.pid,t.shtag,t.path,t.comp");
        sqler.addEqual("t.porid", porid);
        List<SysPortalMenu> list = service.findTree(sqler);
        return R.ok(list);
    }

    @GetMapping("listn")
    @ApiOperation("查询除自身外的菜单树")
    public R getListn( String id) {
        List<SysPortalMenu> list = service.findWithoutItself(id);
        return R.ok(list);
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询菜单详情")
    public R id(@PathVariable String id, HttpServletRequest request) {
        SysPortalMenu main = service.findById(id);
        if(main.getPid()!=null){
            SysPortalMenu parent = service.findById(main.getPid());
            main.setPname(parent.getName());
        }
        return R.ok(main);
    }


    @PostMapping
    @ApiOperation("新增菜单")
    public R post(@RequestBody SysPortalMenu main) {
        System.out.println("插入");
        return R.ok(service.insert(main));
    }

    @PutMapping
    @ApiOperation("修改菜单")
    public R put(@RequestBody SysPortalMenu main) {
        System.out.println("修改");
        return R.ok(service.update(main));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除菜单")
    public R delete(@PathVariable String[] ids) {
        System.out.println(ids);
        return R.ok(service.delete(ids));
    }

    @Autowired
    private SysPortalMenuService service;


}
