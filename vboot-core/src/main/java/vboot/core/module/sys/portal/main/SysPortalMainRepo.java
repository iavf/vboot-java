package vboot.core.module.sys.portal.main;


import org.springframework.data.jpa.repository.JpaRepository;

public interface SysPortalMainRepo extends JpaRepository<SysPortalMain,String> {


}
