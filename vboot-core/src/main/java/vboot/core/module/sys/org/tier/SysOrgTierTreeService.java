package vboot.core.module.sys.org.tier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vboot.core.common.mvc.service.BaseMainService;

import javax.annotation.PostConstruct;

@Service
public class SysOrgTierTreeService extends BaseMainService<SysOrgTierTree> {

    @Autowired
    private SysOrgTierTreeRepo repo;

    @PostConstruct
    public void initDao() {
        super.setRepo(repo);
    }

}

