package vboot.core.module.ass.dict.cate;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.entity.BaseCateEntity;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
@ApiModel("字典分类")
public class AssDictCate extends BaseCateEntity {

    @ApiModelProperty("备注")
    private String notes;
}
