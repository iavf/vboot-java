package vboot.core.module.ass.coge.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.entity.BaseMainEntity;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ApiModel("代码生成-表单")
public class AssCogeForm extends BaseMainEntity {

    @Lob
    @ApiModelProperty("表单大字段")
    private String vform;
}
