package vboot.core.module.ass.dict.cate;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("ass/dict/cate")
@Api(tags = {"字典分类"})
public class AssDictCateApi {

    private String table = "ass_dict_cate";

    @GetMapping
    @ApiOperation("查询字典分类分页")
    public R get(String name) {
        Sqler sqler = new Sqler(table);
        sqler.addLike("t.name", name);
        sqler.addSelect("t.notes,t.ornum");
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询字典分类详情")
    public R getOne(@PathVariable String id) {
        AssDictCate cate = service.findOne(id);
        return R.ok(cate);
    }

    @PostMapping
    @ApiOperation("新增字典分类详情")
    public R post(@RequestBody AssDictCate cate) {
        return R.ok(service.insert(cate));
    }

    @PutMapping
    @ApiOperation("修改字典分类详情")
    public R put(@RequestBody AssDictCate cate) {
        return R.ok(service.update(cate));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除字典分类详情")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private AssDictCateService service;

}
