package vboot.core.module.ass.coge.form;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("ass/coge/form")
@Api(tags = {"在线表单"})
public class AssCogeFormApi {

    @GetMapping
    @ApiOperation("查询在线表单分页")
    public R get(String name) {
        Sqler sqler = new Sqler("ass_coge_form");
        sqler.addLike("t.name", name);
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询在线表单详情")
    public R getOne(@PathVariable String id) {
        AssCogeForm main = service.findOne(id);
        return R.ok(main);
    }

    @PostMapping
    @ApiOperation("新增在线表单")
    public R post(@RequestBody AssCogeForm main) {
        return R.ok(service.insert(main));
    }

    @PutMapping
    @ApiOperation("更新在线表单")
    public R put(@RequestBody AssCogeForm main) {
        return R.ok(service.update(main));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除在线表单")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private AssCogeFormService service;

}
