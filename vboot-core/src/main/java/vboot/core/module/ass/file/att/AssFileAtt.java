package vboot.core.module.ass.file.att;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
@Data
@ApiModel("附件信息")
public class AssFileAtt {
    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    private String id;

    @Column(length = 32)
    @ApiModelProperty("地址")
    private String addre;

    @Column(length = 128)
    @ApiModelProperty("前缀名称")
    private String pname;

    @Column(length = 128)
    @ApiModelProperty("后缀名称")
    private String sname;

    @Column(length = 16)
    @ApiModelProperty("大小")
    private String zsize;

    @Transient
    @ApiModelProperty("缩略图")
    private String zimg;

}
