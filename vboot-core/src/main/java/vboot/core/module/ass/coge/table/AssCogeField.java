package vboot.core.module.ass.coge.table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@ApiModel("代码生成-字段")
public class AssCogeField {

    @Id
    @Column(length = 32)
//    @GeneratedValue(generator = "jpa-uuid")
//    @GenericGenerator(name = "jpa-uuid", strategy = "uuid")
    @ApiModelProperty("主键ID")
    private String id;

    @ApiModelProperty("排序号")
    private Integer ornum;

    @Column(length = 32)
    @ApiModelProperty("字段名称")
    private String name;

    @Column(length = 32)
    @ApiModelProperty("字段描述")
    private String remark;

    @Column(length = 128)
    @ApiModelProperty("备注")
    private String notes;

    @Column(length = 32)
    @ApiModelProperty("字段类型")
    private String type;

    @Column(length = 32)
    @ApiModelProperty("字段长度")
    private String length;

}
