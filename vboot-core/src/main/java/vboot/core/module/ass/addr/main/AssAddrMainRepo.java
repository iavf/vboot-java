package vboot.core.module.ass.addr.main;

import org.springframework.data.jpa.repository.JpaRepository;
import vboot.core.module.ass.addr.prov.AssAddrProv;

public interface AssAddrMainRepo extends JpaRepository<AssAddrMain, String> {

}