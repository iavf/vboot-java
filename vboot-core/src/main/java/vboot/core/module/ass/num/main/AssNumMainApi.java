package vboot.core.module.ass.num.main;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.utils.lang.XstrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("ass/num/main")
@Api(tags = {"编号策略"})
public class AssNumMainApi {

    @GetMapping
    @ApiOperation("查询编号策略分页")
    public R get(String name) {
        Sqler sqler = new Sqler("t.id,t.label,t.name,t.nflag,t.cudat,t.crtim,t.uptim","ass_num_main");
        sqler.addLike("t.name",name);
        sqler.addSelect("t.nupre,t.nunex,t.numod");
        return R.ok(service.findPageData(sqler)) ;
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询编号策略详情")
    public R getOne(@PathVariable String id) {
        AssNumMain main=repo.findById(id).get();
        return R.ok(main);
    }

    @PostMapping
    @ApiOperation("新增编号策略")
    public synchronized R post(@RequestBody AssNumMain main) {
        if(repo.existsById(main.getId())){
            return R.build(201,"编号已存在，请修改编号");
        }
        main.setNflag(true);
        repo.save(main);
        return R.ok();
    }

    @PutMapping
    @ApiOperation("修改编号策略")
    public R pust(@RequestBody AssNumMain main) {
        if(XstrUtil.isBlank(main.getNunex())){
            main.setNflag(true);
        }
        main.setUptim(new Date());
        repo.save(main);
        return R.ok();
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除编号策略")
    public R delete(@PathVariable String[] ids) {
        for (String id : ids) {
            repo.deleteById(id);
        }
        return R.ok();
    }

    @Autowired
    private AssNumMainRepo repo;

    @Autowired
    private AssNumMainService service;
}
