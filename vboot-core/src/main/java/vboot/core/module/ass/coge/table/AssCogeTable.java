package vboot.core.module.ass.coge.table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.entity.BaseMainEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@ApiModel("代码生成-表格")
public class AssCogeTable extends BaseMainEntity {

    @Column(length = 64)
    @ApiModelProperty("实体类")
    private String bunam;//业务名

    @ApiModelProperty("排序号")
    private Integer ornum;

    @Column(length = 64)
    @ApiModelProperty("表描述")
    private String remark;

    @Column(length = 64)
    @ApiModelProperty("继承基类")
    private String baent;

    @Column(length = 32)
    @ApiModelProperty("编辑页类型")
    private String edtyp;

    @Column(length = 32)
    @ApiModelProperty("所属门户ID")
    private String porid;

    @Column(length = 32)
    @ApiModelProperty("上级菜单ID")
    private String pmeid;

    @ApiModelProperty("每行列数")
    private Integer pecol;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "tabid")
    @OrderBy("ornum ASC")
    @ApiModelProperty("字段信息")
    private List<AssCogeField> fields = new ArrayList<>();

    @ApiModelProperty("新增按钮")
    private Boolean addbt;

    @ApiModelProperty("删除按钮")
    private Boolean delbt;

    @ApiModelProperty("导入按钮")
    private Boolean impbt;

    @ApiModelProperty("导出按钮")
    private Boolean expbt;

    @Column(length = 32)
    @ApiModelProperty("路由类型")
    private String rotyp;

    @Column(length = 32)
    @ApiModelProperty("排序字段")
    private String orfie;

}
