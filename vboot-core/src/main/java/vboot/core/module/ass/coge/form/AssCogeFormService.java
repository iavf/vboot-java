package vboot.core.module.ass.coge.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vboot.core.common.mvc.service.BaseMainService;

import javax.annotation.PostConstruct;

@Service
public class AssCogeFormService extends BaseMainService<AssCogeForm> {

    @Autowired
    private AssCogeFormRepo repo;

    @PostConstruct
    public void initDao() {
        super.setRepo(repo);
    }

}

