package vboot.core.module.ass.addr.city;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Getter
@Setter
@ApiModel("城市信息")
public class AssAddrCity extends BaseEntity {


    @Column(length = 32)
    @ApiModelProperty("代码")
    private String code;

    @Column(length = 64)
    @ApiModelProperty("坐标")
    private String cecoo;

    @Column(length = 32)
    @ApiModelProperty("省份")
    private String prov;
}

