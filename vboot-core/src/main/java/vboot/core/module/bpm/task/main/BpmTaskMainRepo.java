package vboot.core.module.bpm.task.main;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BpmTaskMainRepo extends JpaRepository<BpmTaskMain,String> {


}