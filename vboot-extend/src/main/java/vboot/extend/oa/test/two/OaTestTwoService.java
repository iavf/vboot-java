package vboot.extend.oa.test.two;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vboot.extend.oa.test.one.OaTestOne;
import vboot.extend.oa.test.one.OaTestOneRepo;
import vboot.core.common.utils.lang.XstrUtil;

import javax.annotation.Resource;
import java.util.List;


@Service
@Transactional(rollbackFor = Exception.class)
public class OaTestTwoService {

    public OaTestTwo findOne(String id) {
        return demoTwoMapper.selectById(id);
    }

    public List<OaTestTwo> findAll() {
        return demoTwoMapper.selectList(null);
    }

    public String insert(OaTestTwo cust) {
        cust.setId(XstrUtil.getUUID());
        demoTwoMapper.insert(cust);
        return cust.getId();
    }

    public String update(OaTestTwo cust) {
        demoTwoMapper.updateById(cust);
        return cust.getId();
    }

    public int delete(String[] ids) {
        for (String id : ids) {
            demoTwoMapper.deleteById(id);
        }
        return ids.length;
    }

    public void test() {
        //先通过JPA新增一个代理商
        OaTestOne demoMain=new OaTestOne();
        demoMain.setId(XstrUtil.getUUID());
        demoMain.setName("有事务的项目");
        demoOneRepo.save(demoMain);

        //再通过mybatis-plus新增一个项目，并让它报错
        OaTestTwo demoMain2=new OaTestTwo();
        demoMain2.setId(XstrUtil.getUUID()+"XXXXXXXXXXXXXXXX");//超出长度保存出错
        demoMain2.setNotes("有事务的客户");
        demoTwoMapper.insert(demoMain2);
    }

    @Resource
    private OaTestOneRepo demoOneRepo;

    @Resource
    private OaTestTwoMapper demoTwoMapper;


}

