package vboot.extend.oa.test.two;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OaTestTwoMapper extends BaseMapper<OaTestTwo>
{

}