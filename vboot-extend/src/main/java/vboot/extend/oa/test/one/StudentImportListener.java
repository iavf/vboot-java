package vboot.extend.oa.test.one;

import com.alibaba.excel.context.AnalysisContext;
import vboot.core.common.utils.file.excel.ExcelListener;
import vboot.core.common.utils.file.excel.ExcelResult;

public class StudentImportListener implements ExcelListener<Student> {
    @Override
    public ExcelResult<Student> getExcelResult() {
        return null;
    }

    @Override
    public void invoke(Student student, AnalysisContext analysisContext) {
        System.out.println(student);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}
