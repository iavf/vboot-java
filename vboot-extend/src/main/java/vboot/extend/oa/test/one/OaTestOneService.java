package vboot.extend.oa.test.one;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vboot.core.common.mvc.service.BaseMainService;

import javax.annotation.PostConstruct;

@Service
public class OaTestOneService extends BaseMainService<OaTestOne> {



    @Autowired
    private OaTestOneRepo repo;

    @PostConstruct
    public void initDao() {
        super.setRepo(repo);
    }

}
