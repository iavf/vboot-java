package vboot.app.de.supp.main;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.entity.BaseMainEntity;
import vboot.core.module.sys.org.root.SysOrg;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@ApiModel("供应商信息")
public class DeSuppMain extends BaseMainEntity {

    //关联分类表示例
    @Column(length = 32)
    @ApiModelProperty("供应商分类")
    private String catid;

    //流水号示例
    @Column(length = 32)
    @ApiModelProperty("流水号")
    private String senum;

    //数据字典示例
    @Column(length = 32)
    @ApiModelProperty("供应商资质")
    private String grade;

    //ManyToOne示例
    @ManyToOne
    @JoinColumn(name = "opman")
    @ApiModelProperty("经办人")
    private SysOrg opman;

    //ManyToMany示例
    @ManyToMany
    @JoinTable(name = "de_supp_viewer", joinColumns = {@JoinColumn(name = "mid")},
            inverseJoinColumns = {@JoinColumn(name = "oid")})
    @ApiModelProperty("可查看者")
    private List<SysOrg> viewers;

    //ManyToMany示例
    @ManyToMany
    @JoinTable(name = "de_supp_editor", joinColumns = {@JoinColumn(name = "mid")},
            inverseJoinColumns = {@JoinColumn(name = "oid")})
    @ApiModelProperty("可编辑者")
    private List<SysOrg> editors;

    //OneToMany示例
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "maiid")
    @OrderBy("ornum ASC")
    @ApiModelProperty("联系人清单")
    private List<DeSuppContact> contacts = new ArrayList<>();

    //附件示例
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "maiid")
    @ApiModelProperty("附件清单")
    private List<DeSuppAtt> atts = new ArrayList<>();


    //地址选择示例，可根据实际情况存想要的字段
    //region-----地址相关信息-----
    @ApiModelProperty("完整地址")
    private String addre;

    @Column(length = 64)
    @ApiModelProperty("省市区")
    private String adreg;

    @Column(length = 128)
    @ApiModelProperty("省市区以外的详细信息")
    private String addet;

    @Column(length = 32)
    @ApiModelProperty("经纬度")
    private String adcoo;

    @Column(length = 32)
    @ApiModelProperty("省")
    private String adpro;

    @Column(length = 32)
    @ApiModelProperty("市")
    private String adcit;

    @Column(length = 32)
    @ApiModelProperty("区")
    private String addis;
    //endregion
}
