package vboot.app.de.supp.main;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vboot.core.module.ass.num.main.AssNumMainService;
import vboot.core.module.mon.log.oper.Oplog;

@RestController
@RequestMapping("de/supp/main")
@Api(tags = {"供应商管理"})
public class DeSuppMainApi {

    @Oplog("查询供应商分页")
    @GetMapping
    @ApiOperation("查询供应商分页")
    public R get(String name) {
        Sqler sqler = new Sqler("de_supp_main");
        sqler.addLike("t.name", name);
        sqler.addSelect("t.addre,t.senum,t.notes");
        return R.ok(service.findPageData(sqler));
    }

    @Oplog("查询供应商详情")
    @GetMapping("one/{id}")
    @ApiOperation("查询供应商详情")
    public R getOne(@PathVariable String id) {
        DeSuppMain main = service.findOne(id);
        return R.ok(main);
    }

    @Oplog("新增供应商")
    @PostMapping
    @ApiOperation("新增供应商")
    public R post(@RequestBody DeSuppMain main) {
        main.setSenum(numService.getNum("SUPP"));//设置供应商流水号
        return R.ok(service.insert(main));
    }

    @Oplog("更新供应商")
    @PutMapping
    @ApiOperation("更新供应商")
    public R put(@RequestBody DeSuppMain main) {
        return R.ok(service.update(main));
    }

    @Oplog("删除供应商")
    @DeleteMapping("{ids}")
    @ApiOperation("删除供应商")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private DeSuppMainService service;

    @Autowired
    private AssNumMainService numService;

}
