package vboot.app.de.supp.main;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

//主记录OneToMany
@Entity
@Data
@ApiModel("供应商附件")
public class DeSuppAtt
{
    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    private String id;

    @Column(length = 32)
    @ApiModelProperty("存储地址")
    private String addre;

    @Column(length = 128)
    @ApiModelProperty("文件前缀名")
    private String pname;

    @Column(length = 128)
    @ApiModelProperty("文件后缀名")
    private String sname;

    @Column(length = 16)
    @ApiModelProperty("文件大小")
    private String zsize;

    @Transient
    @ApiModelProperty("图片缩略图")
    private String zimg;

    @Column(length = 256)
    @ApiModelProperty("文件全名")
    private String name;

    @Column(length = 256)
    @ApiModelProperty("文件链接地址")
    private String link;

}
