package vboot.app.de.supp.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vboot.core.common.mvc.service.BaseMainService;

import javax.annotation.PostConstruct;

//供应商服务
@Service
public class DeSuppMainService extends BaseMainService<DeSuppMain> {

    @Autowired
    private DeSuppMainRepo repo;

    @PostConstruct
    public void initDao() {
        super.setRepo(repo);
    }

}

