package vboot.app.de.supp.cate;

import org.springframework.data.jpa.repository.JpaRepository;

//供应商分类JPA仓储
public interface DeSuppCateRepo extends JpaRepository<DeSuppCate,String> {


}
