package vboot.app.de.supp.cate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Service;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.service.BaseCateService;

import javax.annotation.PostConstruct;
import java.util.List;

//供应商分类服务
@Service
public class DeSuppCateService extends BaseCateService<DeSuppCate> {

    //获取分类treeTable数据
    public List<DeSuppCate> findTree(Sqler sqler) {
        List<DeSuppCate> list = jdbcDao.getTp().query(sqler.getSql(), sqler.getParams(),
                new BeanPropertyRowMapper<>(DeSuppCate.class));
        return buildByRecursive(list);
    }

    @Autowired
    private DeSuppCateRepo repo;

    @PostConstruct
    public void initDao() {
        super.setRepo(repo);
    }

}
