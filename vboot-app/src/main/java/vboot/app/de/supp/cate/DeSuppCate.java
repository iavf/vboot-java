package vboot.app.de.supp.cate;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.entity.BaseCateEntity;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ApiModel("供应商分类")
public class DeSuppCate extends BaseCateEntity {

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name = "pid")
    @ApiModelProperty("父类别")
    private DeSuppCate parent;

}
