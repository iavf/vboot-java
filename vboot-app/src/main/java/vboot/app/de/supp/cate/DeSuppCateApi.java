package vboot.app.de.supp.cate;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.pojo.Ztree;

import java.util.List;

@RestController
@RequestMapping("de/supp/cate")
@Api(tags = {"供应商分类管理"})
public class DeSuppCateApi {

    private String table = "de_supp_cate";

    @GetMapping("treez")
    @ApiOperation("查询供应商除自身外的分类树")
    public R getTreez(String name, String id) {
        List<Ztree> list = service.tier2Choose(table, id, name);
        return R.ok(list);
    }

    @GetMapping("treea")
    @ApiOperation("查询供应商分类树")
    public R getTreea(String name) {
        List<Ztree> list = service.findTreeList(table, name);
        return R.ok(list);
    }

    @GetMapping("tree")
    @ApiOperation("查询供应商分类列表")
    public R getTree(String name) {
        Sqler sqler = new Sqler(table);
        sqler.addLike("t.name", name);
        sqler.addOrder("t.ornum");
        sqler.addWhere("t.avtag=1");
        sqler.addSelect("t.crtim,t.uptim,t.pid,t.notes");
        List<DeSuppCate> list = service.findTree(sqler);
        return R.ok(list);
    }

    @GetMapping
    @ApiOperation("查询供应商分类分页")
    public R get(String name) {
        Sqler sqler = new Sqler(table);
        sqler.addLike("t.name", name);
        sqler.addSelect("t.notes");
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询供应商分类详情")
    public R getOne(@PathVariable String id) {
        return R.ok(service.findOne(id));
    }

    @PostMapping
    @ApiOperation("新增供应商分类")
    public R post(@RequestBody DeSuppCate main) {
        return R.ok(service.insert(main));
    }

    @PutMapping
    @ApiOperation("更新供应商分类")
    public R put(@RequestBody DeSuppCate main) {
        return R.ok(service.update(main, table));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除供应商分类")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private DeSuppCateService service;

}
